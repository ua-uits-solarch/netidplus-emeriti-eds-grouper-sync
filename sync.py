#!/usr/bin/python26

import os, re, base64, email, hmac, hashlib, urllib, urllib2, ldap, json
#from pprint import pprint

# EDS connection paramaters
eds_user = os.environ['EDS_USER']
eds_base_dn = os.environ['EDS_BASE_DN']
eds_bind_dn = "uid=" + eds_user + ",ou=app users," + eds_base_dn
eds_bind_pw = os.environ['EDS_PW']
eds_search_dn = "ou=people," + eds_base_dn
eds_host = os.environ['EDS_HOST']

# Grouper params
grouper_host = os.environ['GROUPER_WS_HOST']
grouper_base_path = os.environ['GROUPER_BASE_PATH']
grouper_grp_name = os.environ['DEST_GROUP_NAME']
grouper_user = eds_user
grouper_pass = eds_bind_pw


# get all group members from Grouper
req = urllib2.Request("https://%s%s%s/members" % (grouper_host, grouper_base_path, grouper_grp_name), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
try:
    result = urllib2.urlopen(req)
except urllib2.HTTPError as e:
    print e
    exit(1)

data = result.read()
grouper_resp = json.loads(data)
grouper_members = []
if 'wsSubjects' in grouper_resp['WsGetMembersLiteResult']:
    for rec in grouper_resp['WsGetMembersLiteResult']['wsSubjects']:
        if re.search('^\d{12}$',rec['id']) and rec['sourceId'] == "ldap":
            grouper_members.append(rec['id'])

# get eligible emeriti from EDS
ld = ldap.initialize(eds_host)
ld.simple_bind_s(eds_bind_dn, eds_bind_pw)
eds_members = []
for e in ld.search_s( "ou=people," + eds_base_dn, ldap.SCOPE_SUBTREE, '(&(!(objectclass=arizonaEduTestPerson))(eduPersonPrimaryAffiliation=retiree)(employeeRetireeTitle=*Emeritus*)(uid=*))', ['uaid'] ):
    eds_members.append(e[1]['uaid'][0])

# delete all members from Grouper not in EDS results
del_mems = set(grouper_members) - set(eds_members)
for m in del_mems:
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    req = urllib2.Request("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_grp_name, m), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
    req.get_method = lambda: 'DELETE'
    try:
        data = opener.open(req).read()
        grouper_resp = json.loads(data)
        if grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'] != 'SUCCESS':
            print "Error deleting %s : %s" % (m, grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'])
            exit(1)
    except urllib2.HTTPError as e:
        print e
        exit(1)

# add all missing members to Grouper
add_mems = set(eds_members) - set(grouper_members)
for m in add_mems:
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    req = urllib2.Request("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_grp_name, m), None, {'Authorization': 'Basic %s' % base64.b64encode('%s:%s' % (grouper_user, grouper_pass))})
    req.get_method = lambda: 'PUT'
    try:
        data = opener.open(req).read()
        grouper_resp = json.loads(data)
        if grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'] not in ('SUCCESS','SUCCESS_ALREADY_EXISTED'):
            print "Error adding %s : %s" % (m, grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'])
            exit(1)
    except urllib2.HTTPError as e:
        print e
        exit(1)

