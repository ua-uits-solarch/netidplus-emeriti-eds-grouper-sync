# EDS-to-Grouper sync script for Emeriti faculty access to NetID+

Simple Python script that syncs the results of an EDS query (for emeriti faculty) to a Grouper group. This Grouper group is used by NetID+ to permit "override" access for users who are not covered by the normal authorization rules (e.g., student/staff/faculty).

Credentials and relevant environment info are in the ```sync.sh``` file, which is what should be executed (from Cron or however you wish to invoke it).

The Python script has a few module dependencies, which you can see in the ```import``` line in ```sync.py```
