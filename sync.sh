#!/bin/sh

# where the Python script is located
export PY_SCRIPT_DIR=/home/windhamg

# setup environment variable for EDS stuff
export EDS_USER="eds app user username"
export EDS_PW="eds app user password"
export EDS_BASE_DN="dc=eds,dc=arizona,dc=edu"
export EDS_HOST="ldaps://eds.arizona.edu"

# Grouper WS stuff
export GROUPER_WS_HOST="grouper.arizona.edu"
export GROUPER_BASE_PATH="/grouper-ws/servicesRest/json/v2_2_001/groups/"
export DEST_GROUP_NAME='arizona.edu:services:enterprise:netid-plus:emeriti'

${PY_SCRIPT_DIR}/sync.py

